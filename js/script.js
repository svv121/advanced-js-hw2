"use strict";
/*
Коли помилка зустрічається у JavaScript, інтерпретатор припиняє виконання коду та виводить у консоль браузера повідомлення про помилку. Щоб код на JS залишався працездатним, ці помилки має сенс перехоплювати.
try..catch може обробляти лише помилки, які виникають у коректному коді. Такі помилки називають "помилками під час виконання", або "виключеннями".Приклади виключень: некоректні вхідні дані, не виконуються умови функції. try – частина робочого коду, який слід перевірити на наявність помилки (виключення); catch – блок, у якому відбувається обробка помилки; finally – блок, який необов'язковий, але за його наявності виконується у будь-якому разі незалежно від результатів виконання блоку try.
Якщо у try помилок немає, то блок catch(err) пропускається. Якщо у try виникає помилка, то виконання try у ньому переривається, і управління передається блоку catch(err).
Щоб зловити виняток усередині запланованої функції, try..catch повинен знаходитися всередині цієї функції.
Щоб сигналізувати про виняткову ситуацію в коді, використовується інструкція throw, за допомогою якої генерується виключення, виконання поточної функції зупиняється (інструкції після throw не будуть виконані), і управління буде передано в перший блок catch. Якщо використовувати throw разом із try and catch, можна контролювати потік програми та створювати власні повідомлення про помилки.
*/

const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const idRoot = document.querySelector("#root");
const ulInsert = document.createElement('ul');
ulInsert.classList.add("total-list-of-books");

    function reportBooks(entry) {
        return `<li>Book:<ul class="books-item">
                ${Object.entries(entry).map(([key, value]) =>
                `<li> ${key}: ${value}</li>`).join('')}
                </ul></li><br>`
    }

    function CheckBooksComplete(entry) {
        if (!entry.author) {
            throw new Error(`The property 'author' is missing`)
        }
        if (!entry.name) {
            throw new Error(`The property 'name' is missing`);
        }
        if (!entry.price) {
            throw new Error(`The property 'price' is missing`);
        }
        return reportBooks(entry);
    }

    function composeListOfBooks(array) {
        return ulInsert.insertAdjacentHTML("beforeend", (`${array.map(
            entry => {
                try {
                    return CheckBooksComplete(entry)
                } catch (err) {
                    console.warn(err.message);
                }
            }).join('')}`))
    }

composeListOfBooks(books);
idRoot.append(ulInsert);